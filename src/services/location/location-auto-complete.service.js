import camelize from "camelize";
import { mocks } from "../restaurants/mock";
import { locations } from "./location.mock";

export const locationAutoCompleteRequest = (searchTerm) => {
  return new Promise((resolve, reject) => {
    let results = [];
    for (let item in locations) {
      if (item.toLowerCase().includes(searchTerm.toLowerCase())) {
        results.push({ name: item, ...locations[item] });
      }
    }
    // if (results.length == 0) {
    //   reject("No such locations");
    // }
    resolve(results);
  });
};

export const locationAutoCompleteTransform = (results = []) => {
  let mappedResults = [];
  results.map((element) => {
    const geometry = element.results[0].geometry.location;
    let locationName = element.name;

    const firstLetter = locationName[0].toUpperCase();

    const otherLetter = locationName.slice(1).toLowerCase();

    locationName = firstLetter.concat(otherLetter);

    // const locationCord = `${geometry.lat},${geometry.lng}`;
    // const locationObj = camelize(mocks[locationCord]);
    // const locationName = locationObj.results[0].name;
    mappedResults.push({
      lat: geometry.lat,
      lng: geometry.lng,
      name: locationName,
    });
  });
  return mappedResults;
};
