import React, { useState, useContext, useEffect, createContext } from "react";
import { useDispatch } from "react-redux";
import { loaderActions } from "../../store/loader-slice";
import { notificationActions } from "../../store/notification-slice";
import {
  locationAutoCompleteRequest,
  locationAutoCompleteTransform,
} from "./location-auto-complete.service";
import { locationRequest, locationTransform } from "./location.service";

export const LocationContext = createContext();

export const LocationContextProvider = ({ children }) => {
  const [location, setLocation] = useState(null);

  const [locationAutoComplete, setLocationAutoCompleteResults] = useState(null);

  const [keyword, setKeyword] = useState("San Francisco");

  const [autoCompleteKeyword, setAutoComplete] = useState(null);
  const [isLoading, setIsLoading] = useState(false);
  const [error, setError] = useState(false);
  const dispatch = useDispatch();

  const onSearch = (searchKeyword) => {
    setKeyword(searchKeyword);
    setIsLoading(true);
  };

  const setAutoCompleteEmpty = () => {
    setAutoComplete(null);
  };
  const onAutoComplete = (keyword) => {
    setAutoComplete(keyword);
  };

  useEffect(() => {
    if (!autoCompleteKeyword || !autoCompleteKeyword.length) {
      setLocationAutoCompleteResults([]);
      return;
    }
    setIsLoading(true);
    locationAutoCompleteRequest(autoCompleteKeyword)
      .then(locationAutoCompleteTransform)
      .then((result) => {
        setIsLoading(false);
        setLocationAutoCompleteResults(result);
      })
      .catch((err) => {
        dispatch(
          notificationActions.showNotification({
            status: "error",
            message: err,
          })
        );
        setError(err);
        setIsLoading(false);
        setLocationAutoCompleteResults([]);
      });
  }, [autoCompleteKeyword]);

  useEffect(() => {
    if (!keyword.length) {
      return;
    }
    setIsLoading(true);
    setLocationAutoCompleteResults([]);
    locationRequest(keyword.toLowerCase())
      .then(locationTransform)
      .then((result) => {
        setIsLoading(false);
        setLocation(result);
        setAutoComplete(null);
      })
      .catch((err) => {
        dispatch(
          notificationActions.showNotification({
            status: "error",
            message: err,
          })
        );
        setIsLoading(false);
        setError(err);
      });
  }, [keyword]);

  return (
    <LocationContext.Provider
      value={{
        isLoading,
        error,
        location,
        search: onSearch,
        autoComplete: onAutoComplete,
        locationAutoComplete,
        keyword,
        setAutoCompleteEmpty,
      }}
    >
      {children}
    </LocationContext.Provider>
  );
};
