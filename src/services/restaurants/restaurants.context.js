import React, {
  useState,
  useEffect,
  createContext,
  useMemo,
  useContext,
} from "react";
import { useDispatch } from "react-redux";
import { loaderActions } from "../../store/loader-slice";
import { notificationActions } from "../../store/notification-slice";
import { LocationContext } from "../location/location.context";
import { restaurantRequest, restaurantsTransform } from "./restaurants.service";

export const RestaurantsContext = createContext();

export const RestaurantsContextProvider = ({ children }) => {
  const [restaurants, setRestaurants] = useState([]);
  const [isLoading, setIsLoading] = useState(false);
  const [error, setError] = useState(null);
  const { location } = useContext(LocationContext);

  const dispatch = useDispatch();

  const retrieveRestaurants = (location) => {
    setIsLoading(true);
    setRestaurants([]);
    dispatch(loaderActions.showLoader({ backdrop: true }));
    setTimeout(() => {
      restaurantRequest(location)
        .then(restaurantsTransform)
        .then((restaurants) => {
          setIsLoading(false);
          dispatch(loaderActions.hideLoader());
          setRestaurants(restaurants);
        })
        .catch((err) => {
          setIsLoading(false);
          dispatch(loaderActions.hideLoader());
          dispatch(
            notificationActions.showNotification({
              status: "error",
              message: err,
            })
          );
          setError(err);
        });
    }, 1000);
  };
  useEffect(() => {
    if (location) {
      const locationString = `${location.lat},${location.lng}`;
      retrieveRestaurants(locationString);
    }
  }, [location]);

  return (
    <RestaurantsContext.Provider
      value={{
        restaurants,
        isLoading,
        error,
      }}
    >
      {children}
    </RestaurantsContext.Provider>
  );
};
