import {configureStore} from '@reduxjs/toolkit';
import loaderSlice from './loader-slice';
import notificationSlice from './notification-slice';

const store = configureStore({
  reducer: {
    loader: loaderSlice.reducer,
    notification: notificationSlice.reducer,
  },
});

export default store;
