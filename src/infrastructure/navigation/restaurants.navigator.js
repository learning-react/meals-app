import {
  createStackNavigator,
  TransitionPresets,
} from "@react-navigation/stack";
import { RestaurantDetailScreen } from "../../features/restaurants/screens/restaurant-details.screen";
import { RestaurantsScreen } from "../../features/restaurants/screens/restaurants.screen";
const RestaurantStack = createStackNavigator();

const createScreenOptions = ({ route }) => {
  return {
    headerShown: false,
    ...TransitionPresets.ModalPresentationIOS,
  };
};

export const RestaurantsNavigator = () => {
  return (
    <RestaurantStack.Navigator screenOptions={createScreenOptions}>
      <RestaurantStack.Screen name=" " component={RestaurantsScreen} />
      <RestaurantStack.Screen
        name="RestaurantDetail"
        component={RestaurantDetailScreen}
      />
    </RestaurantStack.Navigator>
  );
};
