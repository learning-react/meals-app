export const colors = {
  brand: {
    primary: "#2182bd",
    secondary: "#5282bd",
    muted: "#c6daf7",
  },

  ui: {
    primary: "#262626",
    secondary: "#757575",
    teritary: "#f1f1f1",
    quaternary: "#ffffff",
    disabled: "#dedede",
    error: "#d0421b",
    success: "#138000",
    body: "#FFF5E1",
    icon: "#F36365",
  },

  bg: {
    primary: "#ffffff",
    secondary: "#f1f1f1",
  },

  text: {
    primary: "#262626",
    secondary: "#757575",
    disabled: "#9c9c9c",
    inverse: "#ffffff",
    error: "#d0421b",
    success: "#138000",
  },

  notify: {
    error: "#F8B9B9",
    success: "#DFF2BF",
    info: "#D9EDF7",
    warning: "#FEEFB3",
  },

  notifyText: {
    error: "#D93D25",
    success: "#628C2C",
    info: "#327292",
    warning: "#9F643F",
  },

  loader: {
    primary: "#8a2b07",
    borderColor: "#f3f3f3",
    backdrop: "rgba(0, 0, 0, 0.7)",
  },
};
