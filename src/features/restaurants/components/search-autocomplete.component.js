import React, { useContext } from "react";
import { FlatList, Keyboard, Platform } from "react-native";

import { Card } from "react-native-paper";
import styled from "styled-components/native";
import { MaterialIcons } from "@expo/vector-icons";
import { Text } from "../../../components/typography/text.component";
import { LocationContext } from "../../../services/location/location.context";
import {
  TouchableOpacity,
  TouchableWithoutFeedback,
} from "react-native-gesture-handler";
const AutoComplete = styled(FlatList).attrs({
  contentContainerStyle: {},
})``;

const AutoCompleteWrapper = styled(Card)`
  position: absolute;
  max-height: 200px;
  width: 100%;
  top: 46px;
  z-index: 99;
`;

const ItemWrapper = styled.View`
  flex-direction: row;
  margin-top: 5px;
  padding: 9px 0px 9px 0px;
  align-items: center;
`;

const ItemTitle = styled(Text)`
  margin-left: 22px;
`;

export const SearchAutoComplete = () => {
  const { locationAutoComplete } = useContext(LocationContext);
  const { search } = useContext(LocationContext);

  const Item = ({ item }) => {
    const Touchable =
      Platform.OS == "ios" ? TouchableWithoutFeedback : TouchableOpacity;
    return (
      <Touchable
        onPress={() => {
          search(item.name);
          Keyboard.dismiss();
        }}
      >
        <ItemWrapper>
          <MaterialIcons name="location-pin" size={25} color="#F36365" />
          <ItemTitle variant={"label"}>{item.name}</ItemTitle>
        </ItemWrapper>
      </Touchable>
    );
  };

  return (
    <AutoCompleteWrapper elevation={5}>
      <Card.Content>
        <AutoComplete
          data={locationAutoComplete}
          renderItem={Item}
          keyExtractor={(item) => item.name}
        />
      </Card.Content>
    </AutoCompleteWrapper>
  );
};
