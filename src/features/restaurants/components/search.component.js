import React, { useContext, useEffect, useState } from "react";
import styled from "styled-components/native";

import { Searchbar } from "react-native-paper";
import { LocationContext } from "../../../services/location/location.context";
import { SearchAutoComplete } from "./search-autocomplete.component";
import { theme } from "../../../infrastructure/theme";
const SearchWrapper = styled.View`
  position: relative;
`;

const SearchContainer = styled.View`
  padding: ${(props) => props.theme.space[3]};
  padding-bottom: 0;
  z-index: 99;
`;

export const Search = ({ isFavouritesToggled, onFavouritesToggle }) => {
  const {
    keyword,
    search,
    autoComplete,
    locationAutoComplete: autoCompleteResults,
  } = useContext(LocationContext);

  const [searchKeyword, setSearchKeyword] = useState(keyword);

  useEffect(() => {
    setSearchKeyword(keyword);
  }, [keyword]);

  return (
    <SearchContainer>
      <SearchWrapper>
        <Searchbar
          icon={isFavouritesToggled ? "heart" : "heart-outline"}
          iconColor={theme.colors.ui.icon}
          onIconPress={onFavouritesToggle}
          placeholder="Search for a place"
          value={searchKeyword}
          onSubmitEditing={() => {
            search(searchKeyword);
          }}
          onChangeText={(text) => {
            setSearchKeyword(text);
            autoComplete(text);
          }}
        />
        {autoCompleteResults && autoCompleteResults.length > 0 && (
          <SearchAutoComplete />
        )}
      </SearchWrapper>
    </SearchContainer>
  );
};
