import React, { useState } from "react";
import { ScrollView } from "react-native";
import { List } from "react-native-paper";
import styled from "styled-components/native";
import { SafeArea } from "../../../components/utility/safe-area.component";
import { theme } from "../../../infrastructure/theme";
import { RestaurantInfoCard } from "../components/restaurant-info-card.component";
export const RestaurantDetailScreen = ({ route }) => {
  const { restaurant } = route.params;
  const [breakfastExpanded, setBreakfastExpanded] = useState(false);
  const [lunchExpanded, setLunchExpanded] = useState(false);
  const [dinnerExpanded, setDinnerExpanded] = useState(false);
  const [drinksExpanded, setDrinksExpanded] = useState(false);

  const Accordion = styled(List.Accordion).attrs({
    theme: {
      colors: {
        primary: theme.colors.ui.icon,
      },
    },
  })`
    background-color: ${theme.colors.ui.body};
  `;

  const ListItem = styled(List.Item)`
    background-color: #fff;
    padding: 10px;
  `;

  return (
    <SafeArea>
      <RestaurantInfoCard restaurant={restaurant} />
      <ScrollView>
        <Accordion
          title="Breakfast"
          left={(props) => (
            <List.Icon
              {...props}
              icon="bread-slice"
              color={theme.colors.ui.icon}
            />
          )}
          expanded={breakfastExpanded}
          onPress={() => setBreakfastExpanded(!breakfastExpanded)}
        >
          <ListItem title="Eggs Benedict" />
          <ListItem title="Classic Breakfast" />
        </Accordion>
        <Accordion
          title="Lunch"
          left={(props) => (
            <List.Icon
              {...props}
              icon="hamburger"
              color={theme.colors.ui.icon}
            />
          )}
          expanded={lunchExpanded}
          onPress={() => setLunchExpanded(!lunchExpanded)}
        >
          <ListItem title="Burger w/ Fries" />
          <ListItem title="Steak Sandwich" />
          <ListItem title="Mushroom Soup" />
        </Accordion>
        <Accordion
          title="Dinner"
          left={(props) => (
            <List.Icon
              {...props}
              icon="food-variant"
              color={theme.colors.ui.icon}
            />
          )}
          expanded={dinnerExpanded}
          onPress={() => setDinnerExpanded(!dinnerExpanded)}
        >
          <ListItem title="Spaghetti Bolognese" />
          <ListItem title="Cutlet with Chicken Mushroom Roti" />
          <ListItem title="Steak Fries" />
        </Accordion>
        <Accordion
          title="Drinks"
          left={(props) => (
            <List.Icon {...props} icon="cup" color={theme.colors.ui.icon} />
          )}
          expanded={drinksExpanded}
          onPress={() => setDrinksExpanded(!drinksExpanded)}
        >
          <ListItem title="Coffee" />
          <ListItem title="Tea" />
          <ListItem title="Thumbs Up" />
          <ListItem title="Pepsi" />
          <ListItem title="Cola" />
        </Accordion>
      </ScrollView>
    </SafeArea>
  );
};
