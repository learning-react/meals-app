import React, { useContext, useState } from "react";
import { RestaurantInfoCard } from "../components/restaurant-info-card.component";
import { Spacer } from "../../../components/spacer/spacer.component";
import { SafeArea } from "../../../components/utility/safe-area.component";
import { RestaurantsContext } from "../../../services/restaurants/restaurants.context";
import { Search } from "../components/search.component";
import { Keyboard } from "react-native";
import { LocationContext } from "../../../services/location/location.context";
import { TouchableOpacity } from "react-native";
import { FavouritesBar } from "../../../components/favourites/favourites-bar.component";

import { FavouritesContext } from "../../../services/favourites/favourites.context";
import { RestaurantList } from "../components/restaurant-list.styles";
import { FadeInView } from "../../../components/animations/fade.animation";

export const RestaurantsScreen = ({ navigation }) => {
  const [isRefresh, setRefresh] = useState(false);

  const { restaurants } = useContext(RestaurantsContext);
  const { setAutoCompleteEmpty } = useContext(LocationContext);
  const { favourites } = useContext(FavouritesContext);
  const [isFavouritesToggled, setFavouritesToggle] = useState(false);

  return (
    <SafeArea
      onTouchMove={() => {
        Keyboard.dismiss();
      }}
    >
      <Search
        isFavouritesToggled={isFavouritesToggled}
        onFavouritesToggle={() => setFavouritesToggle(!isFavouritesToggled)}
      />
      {isFavouritesToggled && (
        <FavouritesBar
          favourites={favourites}
          onNavigate={navigation.navigate}
        />
      )}

      <RestaurantList
        data={restaurants}
        onRefresh={() => setRefresh(true)}
        refreshing={isRefresh}
        initialNumToRender={2}
        renderItem={({ item }) => {
          return (
            <TouchableOpacity
              onPress={() => {
                navigation.navigate("RestaurantDetail", { restaurant: item });
                setAutoCompleteEmpty();
              }}
            >
              <Spacer position={"bottom"} size={"large"}>
                <FadeInView>
                  <RestaurantInfoCard restaurant={item} />
                </FadeInView>
              </Spacer>
            </TouchableOpacity>
          );
        }}
        keyExtractor={(item) => item.name}
      />
    </SafeArea>
  );
};
