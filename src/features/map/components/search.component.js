import React, { useContext, useEffect, useState } from "react";
import styled from "styled-components/native";

import { FontAwesome5 } from "@expo/vector-icons";
import { Searchbar } from "react-native-paper";
import { LocationContext } from "../../../services/location/location.context";
import { theme } from "../../../infrastructure/theme";
import { SearchAutoComplete } from "../../restaurants/components/search-autocomplete.component";

const SearchWrapper = styled.View`
  position: relative;
`;

const SearchContainer = styled.View`
  padding: ${(props) => props.theme.space[3]};
  position: absolute;
  z-index: 99;
  top: 30px;
  width: 100%;
`;

export const MapSearch = () => {
  const {
    keyword,
    search,
    autoComplete,
    locationAutoComplete: autoCompleteResults,
  } = useContext(LocationContext);
  const [searchKeyword, setSearchKeyword] = useState(keyword);

  useEffect(() => {
    setSearchKeyword(keyword);
  }, [keyword]);

  return (
    <SearchContainer>
      <SearchWrapper>
        <Searchbar
          icon={() => (
            <FontAwesome5 name="map" size={20} color={theme.colors.ui.icon} />
          )}
          placeholder="Search for a location"
          value={searchKeyword}
          onSubmitEditing={() => {
            search(searchKeyword);
          }}
          onChangeText={(text) => {
            setSearchKeyword(text);
            autoComplete(text);
          }}
        />
        {autoCompleteResults && autoCompleteResults.length > 0 && (
          <SearchAutoComplete />
        )}
      </SearchWrapper>
    </SearchContainer>
  );
};
