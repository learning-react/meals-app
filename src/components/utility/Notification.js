import { Ionicons } from "@expo/vector-icons";
import React, { useState, useEffect } from "react";
import { useDispatch, useSelector } from "react-redux";
import styled from "styled-components/native";
import { notificationActions } from "../../store/notification-slice";
import { Animated, Platform } from "react-native";

const NotificationContainer = styled(Animated.View)`
  position: absolute;
  width: 100%;
  bottom: ${Platform.OS == "ios" ? "80px" : "50px"};
`;

const NotificationWrapper = styled.View`
  background-color: ${(props) => props.theme.colors.notify[props.status]};
  border-radius: 10px;
  padding: 15px;
  flex-direction: row;
`;

const NotificationText = styled.Text`
  text-align: center;
  color: ${(props) => props.theme.colors.notifyText[props.status]};
  font-weight: 500;
  font-size: 15px;
`;

const CloseButton = styled.TouchableOpacity`
  flex: 1;
  flex-direction: row;
  justify-content: flex-end;
`;

export const Notification = () => {
  const [fadeAnim] = useState(new Animated.Value(0));
  const dispatch = useDispatch();
  const notification = useSelector((state) => state.notification.notification);

  useEffect(() => {
    runAnimation();
  }, [notification]);

  function runAnimation() {
    Animated.timing(fadeAnim, {
      toValue: 1,
      duration: 500,
      useNativeDriver: true,
    }).start();
  }

  const onCloseNotification = () => {
    fadeAnim.setValue(0);
    dispatch(notificationActions.clearNotification());
  };

  if (notification) {
    setTimeout(() => {
      onCloseNotification();
    }, 6000);
  }

  return notification ? (
    <NotificationContainer style={{ opacity: fadeAnim }}>
      <NotificationWrapper status={notification.status}>
        <NotificationText status={notification.status}>
          {notification.message}
        </NotificationText>
        <CloseButton onPress={onCloseNotification}>
          <Ionicons name="close" size={20} />
        </CloseButton>
      </NotificationWrapper>
    </NotificationContainer>
  ) : null;
};
